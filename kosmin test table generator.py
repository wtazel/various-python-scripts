print()
print("## Kosmin Test for predicting 800m time - Table ##")
print()
print("Total Distance   |   Potential Time")
print("------------------------------------")

i = 100
while i<=1000 :
    res = 217.77778 - (i * 0.119556)
    minute = int(res / 60)
    sec = int(res % 60)
    if i==1000:
        print(str(i) + "m                 " + str(minute)+"min " + str(sec) + "s")
    else:
        print(str(i) + "m                  " + str(minute)+"min " + str(sec) + "s")
    i+=20
